# raspiradio
### A Python- and mplayer-based Raspberry Pi internet radio

This is my Raspberry Pi project, currently under construction. 

Take a look [at this blog series](http://www.jstevens.co.uk/making-a-raspberry-pi-internet-radio.html)
for details of how it's going. 