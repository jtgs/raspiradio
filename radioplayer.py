import subprocess
import sys
import lcd
import time
import datetime as dt
import RPi.GPIO as gpio
import threading
import logging

class StreamPlayer(object):
    """ Class controlling mplayer """
    def __init__(self):
        self.process = None
        # The volume always starts at 91%
        self.volume = 91
        logging.debug("Initialised StreamPlayer %s" % self)

    # Basic functions required to use mplayer
    def play(self, url):
        """ Start mplayer and play a stream """
        if self.process is not None:
            self.stop()
        opts = ["mplayer", "-quiet", "-playlist", url]
        self.process = subprocess.Popen(opts,
                                        shell=False,
                                        stdout=subprocess.PIPE,
                                        stdin=subprocess.PIPE,
                                        stderr=subprocess.STDOUT)
        logging.debug("Launched stream %s" % url)

    def send_command(self, command):
        """ Send a keystroke to mplayer """
        if self.process is not None:
            self.process.stdin.write(command.encode("utf-8"))
        logging.debug("Sent command %s to player" % command)

    # Helper functions to give sensible names to the keystroke commands
    def stop(self):
        """ Kill mplayer """
        logging.debug("StreamPlayer.stop()")
        self.send_command("q")
        self.process = None

    def pause(self):
        """ Pause or unpause the stream """
        logging.debug("StreamPlayer.pause()")
        self.send_command(" ")

    def volume_up(self):
        """ Turn up the volume """
        logging.debug("StreamPlayer.volume_up()")
        if self.volume < 100:
            self.send_command("*")
            self.volume += 3
        logging.debug("--> volume is now %d", self.volume)
        return self.volume

    def volume_down(self):
        """ Turn down the volume """
        logging.debug("StreamPlayer.volume_down()")
        if self.volume > 0:
            self.send_command("/")
            self.volume -= 3
        logging.debug("--> volume is now %d", self.volume)
        return self.volume

    def mute(self):
        """ Mute or unmute the volume """
        logging.debug("StreamPlayer.mute()")
        self.send_command("m")


class Station(object):
    """ Class representing a radio stream """
    def __init__(self, name, genre, url):
        self.name = name
        self.genre = genre
        self.url = url
        logging.debug("Initialised Station %s" % self)


class Screen(object):
    def __init__(self):
        self.lcd = lcd.LCD()
        self.station = ""
        self.clock = self.ClockThread(self)
        self.clock.start()
        logging.debug("Initialised Screen %s" % self)

    def change_station(self, name):
        logging.debug("Screen.change_station(%s)" % name)
        self.lcd.write("{:10.10} {}".format(name, self.time), 1)
        self.lcd.write(" " * 16, 2)
        self.station = name

    def cleanup(self):
        logging.debug("Screen.cleanup()")
        self.lcd.clear()

    def station_picker(self, name, info):
        logging.debug("Screen.station_picker(%s, %s)" % (name, info))
        self.lcd.write("< {:12.12} >".format(name), 1)
        self.lcd.write("{:16.16}".format(info), 2)

    def display_volume(self, vol):
        logging.debug("Screen.display_volume(%d)" % vol)
        self.lcd.write("   Volume {}%   ".format(vol), 2)
        thread = self.VolumeTimeoutThread(self, 3)
        thread.start()

    def clear_volume(self):
        logging.debug("Screen.clear_volume()")
        self.lcd.write(" " * 16, 2)

    class ClockThread(threading.Thread):
        def __init__(self, parent):
            threading.Thread.__init__(self)
            self.parent = parent
            self.daemon = True
            logging.debug("Initialised ClockThread %s" % self)

        def run(self):
            while True:
                self.parent.time = dt.datetime.now().strftime("%H:%M")
                seconds = dt.datetime.now().second
                self.parent.change_station(self.parent.station)
                time.sleep(60 - seconds)

    class VolumeTimeoutThread(threading.Thread):
        def __init__(self, parent, timeout):
            threading.Thread.__init__(self)
            self._killswitch = threading.Event()
            self.parent = parent
            self.timeout = timeout
            logging.debug("Initialised VolumeTimeoutThread %s" % self)
            # When one of these threads starts, kill the previous one.
            # This ensure we always wait the right amount of time before
            # resetting the display.
            for thread in threading.enumerate():
                if thread.__class__ == self.__class__:
                    thread._killswitch.set()

        def run(self):
            while not self._killswitch.isSet():
                self.timeout -= 0.1
                if self.timeout <= 0:
                    self.parent.clear_volume()
                    break
                self._killswitch.wait(0.1)
            logging.debug("Exiting VolumeTimeoutThread %s" % self)




class Button(object):
    def __init__(self, pin, listener):
        gpio.setmode(gpio.BCM)
        gpio.setup(pin, gpio.IN, pull_up_down=gpio.PUD_UP)
        gpio.add_event_detect(pin,
                              gpio.FALLING,
                              callback=listener,
                              bouncetime=300)
        logging.debug("Initialised Button %s" % self)



