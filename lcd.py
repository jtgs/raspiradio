#!/usr/bin/python

"""
Module to control 16x2 LCD using Raspberry Pi GPIO.

Based on http://www.raspberrypi-spy.co.uk/2012/07/
16x2-lcd-module-control-using-python/
"""

import RPi.GPIO as GPIO
import time

# Define GPIO to LCD mapping
LCD_RS = 7
LCD_E  = 8
LCD_D4 = 25
LCD_D5 = 24
LCD_D6 = 23
LCD_D7 = 18

# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005

class LCD(object):
    def __init__(self):
        # Set up GPIO
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
        GPIO.setup(LCD_E, GPIO.OUT)  # E
        GPIO.setup(LCD_RS, GPIO.OUT) # RS
        GPIO.setup(LCD_D4, GPIO.OUT) # DB4
        GPIO.setup(LCD_D5, GPIO.OUT) # DB5
        GPIO.setup(LCD_D6, GPIO.OUT) # DB6
        GPIO.setup(LCD_D7, GPIO.OUT) # DB7

        # Set up display
        self.send_byte(0x33,LCD_CMD) # 110011 Initialise
        self.send_byte(0x32,LCD_CMD) # 110010 Initialise
        self.send_byte(0x06,LCD_CMD) # 000110 Cursor move direction
        self.send_byte(0x0C,LCD_CMD) # 001100 Display On,Cursor Off, Blink Off
        self.send_byte(0x28,LCD_CMD) # 101000 Data length, number of lines, font size
        self.send_byte(0x01,LCD_CMD) # 000001 Clear display

    def send_byte(self, bits, mode):
        """
        Send a byte to the data pins.

        Params:
         - bits - the data
         - mode = True for character, False for command

        """
        GPIO.output(LCD_RS, mode)

        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits&0x10==0x10:
          GPIO.output(LCD_D4, True)
        if bits&0x20==0x20:
          GPIO.output(LCD_D5, True)
        if bits&0x40==0x40:
          GPIO.output(LCD_D6, True)
        if bits&0x80==0x80:
          GPIO.output(LCD_D7, True)

        self.toggle_enable()

        # Low bits
        GPIO.output(LCD_D4, False)
        GPIO.output(LCD_D5, False)
        GPIO.output(LCD_D6, False)
        GPIO.output(LCD_D7, False)
        if bits&0x01==0x01:
          GPIO.output(LCD_D4, True)
        if bits&0x02==0x02:
          GPIO.output(LCD_D5, True)
        if bits&0x04==0x04:
          GPIO.output(LCD_D6, True)
        if bits&0x08==0x08:
          GPIO.output(LCD_D7, True)

        self.toggle_enable()

    def toggle_enable(self):
        time.sleep(E_DELAY)
        GPIO.output(LCD_E, True)
        time.sleep(E_PULSE)
        GPIO.output(LCD_E, False)
        time.sleep(E_DELAY)

    def clear(self):
        self.send_byte(0x01, LCD_CMD)

    def write(self, message, line):
        """
        Write a string to the display.

        Params:
         - message - the string to write
         - line - 1 or 2, the line to display on

        """

        # Left-justify the message
        message = message.ljust(LCD_WIDTH, ' ')

        if line == 1:
            line_cmd = LCD_LINE_1
        elif line == 2:
            line_cmd = LCD_LINE_2
        else:
            raise ValueError('Line parameter must be 1 or 2.')

        # Set the line
        self.send_byte(line_cmd, LCD_CMD)

        # Send each character
        for char in range(LCD_WIDTH):
            self.send_byte(ord(message[char]), LCD_CHR)



