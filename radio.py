import radioplayer as rp
import sys
import time
import logging
import datetime

class Radio(object):
    """ A radio player with an LCD display. """
    def __init__(self):
        self.stations = []
        self.station = None
        self.browsing_ix = None
        self.playing = False
        self.player = rp.StreamPlayer()
        self.display = rp.Screen()
        self.vol_up_button = rp.Button(17, self.volume_up)
        self.vol_up_button = rp.Button(27, self.volume_down)
        self.ch_next_button = rp.Button(10, self.channel_next)
        self.ch_prev_button = rp.Button(9, self.channel_prev)
        self.ok_button = rp.Button(22, self.ok)
        logging.debug('Initialised Radio object %s' % self)

    def add_station(self, name, genre, url):
        self.stations.append(rp.Station(name, genre, url))
        logging.info('Added station %s' % name)

    def play_station(self, station):
        self.station = station
        logging.info('Playing %s', self.station.name)
        self.playing = True
        self.player.play(self.station.url)
        self.display.change_station(self.station.name)

    def stop(self):
        logging.info('Stopping')
        self.playing = False
        self.player.stop()
        self.display.change_station("")

    def list_stations(self):
        for ix, station in enumerate(self.stations):
            print "#{ix} - {name} ({genre})".format(ix=ix,
                                                     name=station.name,
                                                     genre=station.genre)
        logging.info("Listed %d stations" % len(self.stations))

    def volume_up(self, pin):
        new_vol = self.player.volume_up()
        logging.info("volume_up: now %d" % new_vol)
        self.display.display_volume(new_vol)

    def volume_down(self, pin):
        new_vol = self.player.volume_down()
        logging.info("volume_down: now %d" % new_vol)
        self.display.display_volume(new_vol)

    def channel_next(self, pin):
        logging.info("channel_next")
        if self.browsing_ix is None:
            # First press of channel select buttons
            # Set current station as the start point
            self.browsing_ix = self.stations.index(self.station)

        self.browsing_ix += 1
        next_station = self.stations[self.browsing_ix % len(self.stations)]
        if next_station:
            self.display.station_picker(next_station.name, next_station.genre)

    def channel_prev(self, pin):
        logging.info("channel_prev")
        if self.browsing_ix is None:
            # First press of channel select buttons
            # Set current station as the start point
            self.browsing_ix = self.stations.index(self.station)

        self.browsing_ix -= 1
        next_station = self.stations[self.browsing_ix % len(self.stations)]
        if next_station:
            self.display.station_picker(next_station.name, next_station.genre)

    def ok(self, pin):
        # OK button has three actions:
        # - if browsing stations, select current new station
        # - if on, turn radio off
        # - if off, turn radio on
        logging.info("ok")
        if self.browsing_ix is not None:
            logging.info("-> select new station")
            self.play_station(self.stations[self.browsing_ix % len(self.stations)])
            self.browsing_ix = None
        elif self.playing:
            logging.info("-> turn off")
            self.stop()
        else:
            logging.info("-> turn on")
            if self.station is None:
                self.station = self.stations[0]
            self.play_station(self.station)



if __name__ == "__main__":
    logname = "radio.{}.log".format(datetime.datetime.now().strftime("%d%m%y-%H%M"))
    logging.basicConfig(filename=logname, level=logging.DEBUG)
    logging.info('Started')

    radio = Radio()
    radio.add_station("Capital",
                      "Top 40",
                      "http://media-ice.musicradio.com/CapitalMP3.m3u")
    radio.add_station("Lincs FM",
                      "Current/Classic Hits",
                      "http://lincs.planetwideradio.com:8000/lincsfmaac.m3u")
    radio.add_station("Heart",
                      "Adult Contemporary",
                      "http://media-ice.musicradio.com/HeartLondonMP3.m3u")
    radio.add_station("BBC R1",
                      "National",
                      "http://www.listenlive.eu/bbcradio1.m3u")
    radio.add_station("BBC R2",
                      "National",
                      "http://www.listenlive.eu/bbcradio2.m3u")
    radio.add_station("BBC R4",
                      "National Talk",
                      "http://www.listenlive.eu/bbcradio4.m3u")

    print "Radio started."
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            radio.player.stop()
            print "Goodbye."
            radio.display.cleanup()
            logging.info('Exited')
            sys.exit(0)
